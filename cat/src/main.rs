use std::env;
use std::fs;
use std::io::{Read};
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    // if no arguments at all, read input from stdin and print it to stdout
    if args.len() == 1 {
        print!("{}", get_string_from_standard_input());
        process::exit(0);
    }


    let mut file_names: Vec<String> = Vec::new();
    let mut stdin_needed: bool = false;
    let mut stdin_place: i16 = -1;
    let mut show_ends: bool = false;
    let mut show_numbers: bool = false;
    let mut show_nonblank_numbers: bool = false;
    let mut squeeze_blank: bool = false;



    for i in 1..args.len() {

        match &args[i][..] {
            "-" => {
                stdin_needed = true;
                stdin_place = i as i16;
            },
            "-b" | "--number-nonblank" => show_nonblank_numbers = true,
            "-E" | "--show-ends" => show_ends = true,
            "-n" | "--number" => show_numbers = true,
            "-s" | "--squeeze-blank" => squeeze_blank = true,
            "--help" => {
                print_help();
                process::exit(0);
            },
            "--version" => {
                print_version();
                process::exit(0);
            },
            _ => file_names.push(String::from(&args[i]))
        }

        if show_nonblank_numbers == true && show_numbers == true {
            show_nonblank_numbers = false;
        }

    }

    if file_names.len() > 0 {
        for i in 0..file_names.len() {
            if stdin_place == i as i16 && stdin_needed {
                print!("{}", get_string_from_standard_input());
            }
    
            let mut line_number: i16 = 0;
    
            let string_from_file = get_string_from_file(String::from(&file_names[i]));
        
            for line in string_from_file.lines() {
                if line == "" && squeeze_blank {
                    continue;
                }
                if show_numbers && !show_nonblank_numbers {
                    print!("{}. ", line_number);
                    line_number = line_number + 1;
                }
                if show_nonblank_numbers {
                    if line != "" {
                        print!("{}. ", line_number);
                        line_number = line_number + 1;
                    }
                }
                print!("{}", line);
                if show_ends {
                    print!("$");
                }
                print!("\n");
            }
        }
    } else {
        if stdin_needed {
            print!("{}", get_string_from_standard_input());
        }
    }
}


fn get_string_from_standard_input() -> String {
    let mut buffer = String::new();
    match std::io::stdin().read_to_string(&mut buffer){
        Ok(_str) => return buffer,
        Err(err) => panic!("Error reading from stdin. Error msg: {}", err)
    };
}

fn get_string_from_file(file_name: String) -> String {
    let file_name_copy = String::from(&file_name);
    match fs::read_to_string(file_name) {
        Ok(str) => return String::from(str),
        Err(err) => panic!("Error opening file {}. Error msg: {}", String::from(file_name_copy), err)
    };
}

fn print_help() {
    println!("help");
}

fn print_version() {
    println!("version");
}